let baseUrl = '';

let domain = 'www.yataiunion.com';
let imgcdn = 'imgcdn.yataiunion.com';  // imgcdn.yataiunion.com

// let domain = 'sra.mogoie.com'
// let imgcdn = 'sraimgcdn2.mogoie.com'
let picture = 'http://'+ imgcdn +'/Uploads/major/'; // 静态文件服务器域名
let staticUrl = 'https://'+ domain +'/Uploads/singleSale/static'; // 静态文件服务器域名
let staticImg = 'http://'+ imgcdn +'/Uploads/Img/';
let staticTab = 'http://'+ imgcdn +'/Uploads/tabbar/';
let imgUrl = 'http://'+ imgcdn; // 图片域名

let echatUrl = 'https://www.yataiunion.com/'; // 客服系统域名

// 开发环境
if(process.env.NODE_ENV === 'development'){
    baseUrl = 'https://'+ domain +'/index.php'
}
// 生产环境
if(process.env.NODE_ENV === 'production'){
    baseUrl = 'https://'+ domain +'/index.php'
}
export default {
	baseUrl,        // 接口请求地址
	imgUrl,			// 图片域名地址
	staticUrl,		// 文件服务器域名
	echatUrl,		// 客服系统域名
	staticImg,
	staticTab,
	picture
}