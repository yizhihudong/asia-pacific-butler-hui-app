import config from '@/common/config.js';
import store from '@/store/index.js'

// 图片上传
function uploadFile(file) {
	return new Promise((resolve, reject) => {
		const uploadTask = uni.uploadFile({
			url: config.baseUrl + `/zzjApp/uploade?bid=${store.state.bid}`,
			filePath: file,
			name: 'file',
			dataType: "json",
			success: function(uploadFileRes) {
				if (uploadFileRes.data) {
					 if(typeof uploadFileRes.data != 'object'){
						  uploadFileRes.data = JSON.parse(uploadFileRes.data)
					 }
					resolve(JSON.parse(uploadFileRes.data));
				}
				
			},
			fail: function(res) {
				console.log(res)
			},
			complete: function() {

			}
		})
	})
}
async function chooseImage(count){
		return new Promise((resolve,reject)=>{
			 uni.chooseImage ({
				count,
				success:async (res)=>  {
					const tempFilePaths = res.tempFilePaths;
					let arr = [];
					for(const item of tempFilePaths){
						let result = await uploadFiles(item);
						let imgItem = result.url.substring(17,result.url.length);
						arr.push(imgItem)
					}
					resolve(arr);
				}
			})
		})
	}


function uploadFiles(file) {
	return new Promise((resolve, reject) => {
		const uploadTask = uni.uploadFile({
			url: config.baseUrl + `/zzjApp/uploade?bid=${store.state.bid}`,
			filePath: file,
			name: 'file',
			dataType: "json",
			success: function(uploadFileRes) {
				if (uploadFileRes.data) {
					let jsonString = uploadFileRes.data;
					let obj = eval('(' + jsonString + ')');  
					resolve(obj);
				}
				
			},
			fail: function(res) {
				console.log(res)
			},
			complete: function() {

			}
		})
	})
}

function richTxt(html) {
	var capture = html,
		array = [];
	var i = -1;
	if (html) {
		html = capture.replace(/src=\s*['"]([^'"]+)[^>]*>/gi, function(match, capture) {
				i++;
				if (capture.indexOf('http') > -1) {
					array.push(capture);
					capture = ' src="' + capture +
						'" style="max-width: 100%; height: auto; margin: 0 auto; display: block;" data-index=' + i +
						' />';
					return capture
				} else {
					array.push(config.imgUrl + capture);
					capture = ' src="' + config.imgUrl + capture +
						'" style="max-width: 100%; height: auto; margin: 0 auto; display: block;" data-index=' + i +
						' />';
					return capture
				}
			})
			.replace(/<section/g, '<div').replace(/\/section>/g, '/div>')
			.replace(/<o:p>/g, '').replace(/<\/o:p>/g, '')
			.replace(/<font/g, '<span').replace(/\/font>/g, '/span>')
			.replace(/<table/g, '<table style="max-width:100%;"')
		// html = new HTMLParser(html.trim());
	}
	var obj = {
		html: html,
		imgs: array
	};
	return obj;
}


// 拨打电话  0.1
function call() {
	let phone = uni.getStorageSync('startUp').service_tel;
	if (phone) {
		uni.makePhoneCall({
			phoneNumber: phone
		})
	} else {
		uni.showToast({
			title: "很抱歉，暂未配置联系电话！",
			icon: 'none'
		})
	}
}

// 拨打电话 0.2
function tel(tel) {

	if (tel) {
		uni.makePhoneCall({
			phoneNumber: tel,
			success(res) {
				console.log(res);
			},
			fail(err) {
				console.log(err);
			}
		})
	} else {
		uni.showToast({
			title: "很抱歉，暂未配置联系电话！",
			icon: 'none'
		})
	}
}


function toUrl(url) {
	uni.navigateTo({
		url: url
	})
}

function rectTo(url) {
	uni.redirectTo({
		url: url
	})
}

function pageHome(){
	uni.switchTab({
		url:'/pages/index/index'
	})
}

function pageTab(url){
	uni.switchTab({
		url
	})
}

function back(time) {
	uni.navigateBack({
		delta: time ? time : 1
	})
}


function timeFormat(time, format) {
	// 	 * 时间戳转化为年 月 日 时 分 秒 
	// 	 * number: 传入时间戳 
	// 	 * format：返回格式，支持自定义，但参数必须与formateArr里保持一致 
	// 	 */
	var formateArr = ['Y', 'M', 'D', 'h', 'm', 's'];
	var returnArr = [];
	if(time && time.length == 10){
		var date = new Date(time * 1000)
	}else{
		var date = new Date(time)
	}
	var getYear = date.getFullYear();
	var getMonth = (date.getMonth() + 1) >= 10 ?(date.getMonth() + 1) : '0' + (date.getMonth() + 1);
	var getDate = date.getDate() >= 10 ? date.getDate() : '0' + date.getDate();
	var getHours = date.getHours() >= 10 ? date.getHours() : '0' + date.getHours();
	var getMinutes = date.getMinutes() >= 10 ? date.getMinutes() : '0' + date.getMinutes();
	var getSeconds = date.getSeconds() >= 10 ? date.getSeconds() : '0' + date.getSeconds()

	returnArr.push(getYear)
	returnArr.push(getMonth)
	returnArr.push(getDate)
	returnArr.push(getHours)
	returnArr.push(getMinutes)
	returnArr.push(getSeconds)
	for (var i in returnArr) {
		format = format.replace(formateArr[i], returnArr[i])
		// 2021-M-D = 2021-M-D.replace(m,getMonth)
		// 2021-12-D = 2021-12-D.replace(D,getDate)
		// console.log(format,'format')

	}
	return format
}







// 图片预览
function previewImage(i, imgArry) {
	if (imgArry) {
		let arr = [];
		for (let i in imgArry) {
			arr.push(config.imgUrl + imgArry[i].cover)
		}
		uni.previewImage({
			current: i,
			urls: arr,
			indicator: "number",
			loop: true
		})
	}
}

function previewImage_content(i, imgArry) {
	if (imgArry) {
		let arr = [];
		for (let i in imgArry) {
			arr.push(config.imgUrl + imgArry[i].content)
		}
		uni.previewImage({
			current: i,
			urls: arr,
			indicator: "number",
			loop: true
		})
	}
}


function previewImage_num(i, imgArry) {
	if (imgArry) {
		let arr = [];
		for (let i in imgArry) {
			arr.push(config.imgUrl + imgArry[i].img)
		}
		uni.previewImage({
			current: i,
			urls: arr,
			indicator: "number",
			loop: true
		})
	}
}

function ImageNum(i, imgArry) {
	if (imgArry) {
		let arr = [];
		for (let i in imgArry) {
			arr.push(config.imgUrl + imgArry[i])
		}
		uni.previewImage({
			current: i,
			urls: arr,
			indicator: "number",
			loop: true
		})
	}
}


function BarTitle(title){
	uni.setNavigationBarTitle({
		title
	})
}

function toGoods(goods_id,store_id) {
	if(store_id){
		uni.navigateTo({
			url:`/pages/goods/goods_detail?goodsid=${goods_id}&shop_id=${store_id}`
		})
	}else{
		uni.navigateTo({
			url: '/pages/goods/goods_detail?goodsid=' + goods_id
		})
	}
}


function istoGoods(type,goods_id,lesson_id) {
	if(type == 13){
		uni.navigateTo({
			url: '/pages/ply/courseDetail?lid=' + lesson_id
		})
	}else{
		uni.navigateTo({
			url: '/pages/goods/goods_detail?goodsid=' + goods_id
		})
	}
	
}
function backHome() {
	uni.switchTab({
		url: `/pages/index/index`
	})
	
}

export default {
	uploadFile,
	uploadFiles,
	richTxt,
	call,
	tel,
	toUrl,
	rectTo,
	back,
	timeFormat,
	previewImage,
	previewImage_num,
	ImageNum,
	previewImage_content,
	pageHome,
	BarTitle,
	pageTab,
	chooseImage,
	toGoods,
	istoGoods,
	backHome
}
