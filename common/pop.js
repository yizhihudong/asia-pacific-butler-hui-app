import config from '@/common/config.js'
import store from '@/store/index.js'
import $http from '@/common/request.js'
import common from '@/common/common.js'
const pop = {


	showToastTxt(msg, state = 1, url) {
		if (state == 1) {
			uni.showToast({
				title: msg,
				icon: 'none',
			})
		} else if (state == 2) {
			uni.showToast({
				title: msg,
				mask: true,
				icon: 'none',
				success: () => {
					setTimeout(() => {
						uni.navigateBack({
							delta: 1
						})
					}, 800)

				}
			})
		} else if (state == 3) {
			uni.showToast({
				title: msg,
				mask: true,
				icon: 'none',
				success: () => {
					setTimeout(() => {
						uni.redirectTo({
							url: url
						})
					}, 800)

				}
			})
		} else if (state == 4) {
			uni.showToast({
				title: msg,
				icon: 'none',
				mask: true,
				duration: 2000
			})
		} else if (state == 5) {
			uni.showToast({
				title: msg,
				icon: 'none',
				mask: true,
				duration: 3000
			})
		}
	},




	showLoading(msg) {
		uni.showLoading({
			title: msg,
			mask: true,
		})
	},
	hideLoading() {
		uni.hideLoading();
	},

	model(title, content = '', confirmText = "确定") {
		return new Promise((resolve, reject) => {
			uni.showModal({
				title,
				content,
				cancelColor: '#cccccc',
				confirmColor: store.state.color,
				confirmText,
				success(res) {
					resolve(res.confirm)
				},
				fail(err) {
					reject(err)
				}
			})
		})
	},
	noModel(title, content = '', confirmText = "确定") {
		return new Promise((resolve, reject) => {
			uni.showModal({
				title,
				content,
				cancelColor: '#cccccc',
				confirmColor: store.state.color,
				confirmText,
				showCancel: false,
				success(res) {
					resolve(res.confirm)
				},
				fail(err) {
					reject(err)
				}
			})
		})
	},

	modelY(title, content = '', confirmText = "确定") {
		return new Promise((resolve, reject) => {
			uni.showModal({
				title,
				content,
				cancelColor: '#cccccc',
				confirmColor: store.state.color,
				cancelText: '稍后完善',
				confirmText,
				success(res) {
					resolve(res.confirm)
				},
				fail(err) {
					reject(err)
				}
			})
		})
	},


	picture(imgs) {
		return config.picture + imgs
	},

	imgUrls(imgs) {
		const reg = /^(http|https)/;
		if (!imgs) return;
		if (imgs.search(reg) != -1) return imgs;
		return config.imgUrl + imgs
	},
	staticImg(imgs) {
		return config.staticImg + imgs
	},
	staticTab(imgs) {
		return config.staticTab + imgs
	},
	navBack() {
		setTimeout(() => {
			uni.navigateBack({
				delta: 1
			})
		}, 600)
	},


	getViewInfo(selector) {
		return new Promise((resolve) => {
			const query = uni.createSelectorQuery().in(this);
			query.select(selector).boundingClientRect(data => {
				resolve(data)
			}).exec();
		})
	},



	pageRela(url) {
		uni.reLaunch({
			url
		})
	},

	tabbar() {
		uni.reLaunch({
			url: '/pages/tabbar/tabbar'
		})
	},

	pageScrollTo(scrollTop = 0, duration = 0) {
		uni.pageScrollTo({
			scrollTop,
			duration
		})
	},

	setClip(text) {
		console.log(text);
		if (!text) return;
		uni.setClipboardData({
			data: text,
			success() {
				pop.showToastTxt('复制成功')
			},
			fail(err) {
				console.log(err, 'err');
				pop.showToastTxt('复制失败')
			}
		})
	},

	saveImage(path) {
		if (!path) return;
		uni.downloadFile({
			url: path,
			success(res) {
				const filePath = res.tempFilePath;
				uni.saveImageToPhotosAlbum({
					filePath,
					success(result) {
						pop.showToastTxt('保存成功')
					},
					fail() {
						pop.showToastTxt('保存失败')
					}
				})
			},
			fail() {
				pop.showToastTxt('保存失败')
			}
		})
	},
	openLocation(item) {
		if (!item.pointx) return;
		const latitude = item.pointy;
		const longitude = item.pointx;
		uni.openLocation({
			latitude: Number.parseFloat(latitude),
			longitude: Number.parseFloat(longitude),
			name: item.name,
			address: item.address,
			success() {
				console.log('打开成功');
			},
			fail(err) {
				console.log(err);
				uni.showToast({
					title: "打开失败!",
					icon: 'none',
				})
			}
		})
	},

	bannelPage(item) {
		if (item.website_list_type == 1) return this.toUrl(
			`/essay/detailPage?website_list_id=${item.website_list_id}`)
		if (item.website_list_type == 2) return this.toUrl(`/essay/ess?website_list_id=${item.website_list_id}`)
		if (item.website_list_type == 4) return this.toUrl(`/essay/webView?url=${item.website_list_url}`)
		if (item.website_list_type == 7 && item.website_list_url) return this.toUrl(item.website_list_url)
		this.toUrl(`/essay/ess?website_list_id=${item.website_list_id}`)
	},


	//  自定义跳转路径
	PageUrl(item) {
		if (!item.type) return;
		let id = item.classify_id || item.id;
		if (item.type == 4) { // 文章列表
			this.toUrl(`/pages/List/List?listId=${id}`)
		}
		if (item.type == 5) { // 文章详情页
			this.toUrl(`/User/details?id=${id}`)
		}
		if (item.type == 12) { // 客服
			if (!store.state.vid) return this.toUrl(`/pages/login/login`)
			this.toUrl(`/pages/concat/concat`)
		}
		if (item.type == 15) { // 视频号
			if (item.website_list_type == 13) { // 视频号首页
				wx.openChannelsUserProfile({
					finderUserName: item.finderUserName,
				})
			}
			if (item.website_list_type == 14) { // 视频号视频
				wx.openChannelsActivity({
					finderUserName: item.finderUserName,
					feedId: item.feedId
				})
			}
			if (item.website_list_type == 15) { // 视频号直播
				wx.openChannelsLive({
					finderUserName: item.finderUserName
				})
			}
			if (item.website_list_type == 16) { // 视频号活动页
				wx.openChannelsEvent({
					finderUserName: item.finderUserName,
					eventId: item.eventId
				})
			}
		}
		if (item.type == 16) { // 打电话
			uni.makePhoneCall({
				phoneNumber: item.website_list_url
			})
		}
		if (item.type == 17) { // 导航
			uni.openLocation({
				latitude: Number.parseFloat(item.seatData.lat),
				longitude: Number.parseFloat(item.seatData.lng),
				address: item.website_list_url,
				success(res) {
					console.log('打开成功');
				},
				fail(err) {
					console.log(err);
				}
			})
		}
		if (item.type == 18) { // 调查问卷
			wx.openEmbeddedMiniProgram({
				appId: item.appID,
				path: item.website_list_url,
			});
		}
		if (item.type == 20) { // 分类
			this.toUrl(`/pages/EventsClass/EventsClass?cate_id=${id}`)
		}
		if (item.type == 21) { // 活动详情
			this.toUrl(`/pages/EventsDetails/EventsDetails?event_id=${id}`)
		}

	},


	Details(goods_id) {
		uni.navigateTo({
			url: `/pages/Details/Details?goods_id=${goods_id}`
		})
	},
	chooseAddress() {
		return new Promise((resolve, reject) => {
			uni.chooseAddress({
				success(res) {
					resolve(res);
				},
				fail(err) {
					reject(err)
				}
			})
		})
	},
	RandomAvatar() {
		const avatarArr = [
			'/Uploads/xiangqing/avatar1.jpg',
			'/Uploads/xiangqing/avatar2.jpg',
			'/Uploads/xiangqing/avatar3.jpg',
			'/Uploads/xiangqing/avatar4.jpg',
		];
		const randomAvatar = avatarArr[Math.floor(Math.random() * avatarArr.length)];
		return randomAvatar;
	},
	//  支付
	getPayment(item) {
		return new Promise((resolve, reject) => {
			uni.requestPayment({
				provider: 'wxpay',
				orderInfo: item,
				timeStamp: item.timeStamp,
				nonceStr: item.nonceStr,
				package: item.package,
				signType: item.signType,
				paySign: item.paySign,
				success: (res) => {
					resolve(res)
				},
				fail: (err) => {
					reject(err);
				}
			})
		})
	},

	isLogin() {
		if (!store.state.vid) {
			common.toUrl(`/pages/login/login`);
			return false;
		}
		return true;
	},

	toStore(item) {
		if (!item || !item.shop_id) return;
		this.toUrl(`/pages/store/store?shop_id=${item.shop_id}`)
	},


	scanCode() {
		return new Promise((resolve, reject) => {
			uni.scanCode({
				success(res) {
					resolve(res);
				},
				fail(err) {
					reject(err)
				}
			})
		})
	},
	SaveImg(url) {
		return new Promise((resolve, reject) => {
			uni.downloadFile({
				url,
				success(res) {
					uni.saveImageToPhotosAlbum({
						filePath: res.tempFilePath,
						success(res2) {
							resolve(res2.path)
						},
						fail(err2) {
							reject(err2)
						}
					})
				},
				fail(err) {

				}
			})
		})
	},

	getLocation() {
		console.log('10000');
		uni.getLocation({
			type: 'wgs84',
			isHighAccuracy: true,
			success: function(res) {
				store.commit('Position', res)
			},
			fail: function(err) {
				console.log(err, 'err');
			}
		})
	},
	bindStore(shop_id) {
		return new Promise((resolve, reject) => {
			$http.post({
				url: '/SRA_userIntegral/bindStore',
				data: {
					shop_id
				}
			}).then(res => {
				if (res.code != 200) return resolve(false)
				store.commit('SetBindState', true)
				resolve(true)
			})
		})
	},

	// 取消支付
	cancelOrder(item) {
		return new Promise((resolve, reject) => {
			pop.model('提示', '确定要取消订单吗？').then(res => {
				if (!res) return;
				$http.post({
					url: '/zzj_singleSaleApi/delOrder',
					data: {
						order_id: item.order_id
					}
				}).then(res => {
					if (res.errcode != 100) return resolve(false);
					resolve(true)
				})
			})
		})
	},
}


export default pop