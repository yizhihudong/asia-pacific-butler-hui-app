
import config from '@/common/config.js';
import store from '@/store/index.js';


let source = ''; //访问端/来源 1微信公众号 2微信小程序 3支付宝小程序 4百度小程序 5头条小程序 6抖音小程序 7快应用
// #ifdef H5
let ua = window.navigator.userAgent.toLowerCase()
if (ua.match(/MicroMessenger/i) == 'micromessenger') {
	source = 1
} else {
	source = 8
}
// #endif
// #ifdef MP-WEIXIN
source = 2
// #endif
// #ifdef MP-ALIPAY
source = 3
// #endif
// #ifdef MP-BAIDU
source = 4
// #endif
// #ifdef MP-TOUTIAO
source = 6
// #endif
// #ifdef QUICKAPP-WEBVIEW-UNION || QUICKAPP-WEBVIEW-HUAWEI || QUICKAPP-WEBVIEW
source = 7
// #endif
// #ifdef APP-PLUS
source = 9
// #endif
let systemData = {
	system: '',     //操作系统版本
	platform: '',   //操作系统类型  ios  android
	brand: '',      //手机品牌
	model: ''       //手机型号
}

let addressData = {
	longitude: 0,   //用户当前经度范围，0-180度 未获取到为 0
	latitude: 0     //用户当前纬度范围，0-90 未获取到为 0
}
// 获取位置经纬度
// uni.getLocation({  
// 	type: 'wgs84',
// 	isHighAccuracy:true,
// 	success: function(res) {
// 		store.commit('Position',res)
// 		addressData = res; 
// 	},
// 	fail:function(err){
// 		console.log(err,'err');
// 	}
// })
// 获取设备信息
// uni.getSystemInfo({ 
// 	success: (res) => {
		
// 		uni.setStorageSync('systemData', res)
// 		systemData = res
// 	}
// })

function request (url, data, method)  {
	let longitude = store.state.position.longitude;
	let latitude = store.state.position.latitude;
    data = Object.assign({}, {
		 source: 9,                    // 访问来源
	   system: systemData.system,         // 操作系统版本
	   platform: systemData.platform,	  // 操作系统类型  ios  android
	   longitude:longitude,               // 位置经纬度
	   latitude:latitude,
	   v_id:store.state.vid,
	   vId:store.state.vid,
	   bid:data && data.bid?data.bid:store.state.bid,
	   VeriCode:data && data.bid?data.bid:store.state.bid,
    }, data)
	let Url;
	const reg = /^(http|https)/;
	if(url.search(reg) != -1){
		Url = url;
	}else{
		Url = config.baseUrl + url
	};
	return new Promise((resolve, reject) => {
		uni.request({
			url: Url,
			method: method,
			data,
			dataType: "json",
			header: {
				'content-type': 'application/x-www-form-urlencoded',
			},
			success: function(resp) {
				resolve(resp.data);
			},
			fail: function(resp) {
				reject(resp);
			}
		})
	})
}

function post(obj) {
    return request(obj.url, obj.data, 'POST');
}

function get(obj) {
    return request(obj.url, obj.data, 'GET');
}

export default {
	post,
	get
}