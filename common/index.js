import request from '@/common/request.js'
import pop from '@/common/pop.js'
import store from '@/store/index.js'


function getWXOpenid(){
	return new Promise((resolve, reject) => {
		uni.getProvider({
			service: "oauth",
			success(res) {
				uni.login({
					provider: res.provider[0],
					success(req) {
						request.get({
							url: '/zzj_singleSaleApi/getOpenId',
							data: {
								js_code: req.code,
								bid:store.state.bid
							}
						}).then(res2 => {
							if (res2.code != '100') return pop.showToast(res2.msg);
							 let StoreData = res2.data;
							 if(StoreData.session_key) store.commit('getUser',{session_key:StoreData.session_key});
							 if(StoreData.openid) store.commit('getUser',{openid:StoreData.openid});
							 store.commit('getUser',{vid:StoreData.vid});
							 if(StoreData.userInfo) store.commit('getUser',{userInfo:StoreData.userInfo});
							resolve(StoreData.openid)
						})
					}
				})
			}
		})
	})
}


function getVid(openid){
		return new Promise((resolve, reject) => {
			request.post({
				url: '/zzj_singleSaleApi/obtainVid',
				data: {
					openid,
					bid: store.state.bid,
				}
			}).then(res => {
				if (res.errcode !== '100') return pop.showToast(res.msg);
				let StoreData = res;
				if (StoreData.v_id && StoreData.v_id > 0) store.commit('getUser',{vid:StoreData.v_id});
				if (StoreData.open_id)  store.commit('getUser',{openid:StoreData.open_id}); 
			})
		})
}






export default {
	getWXOpenid,
	getVid,
}
