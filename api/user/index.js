import request from '../request.js';
import pop from '@/common/pop.js'
import store from '@/store/index.js'

export default {

	getOpenid() {
		return new Promise((resolve, reject) => {
			uni.getProvider({
				service: "oauth",
				success: (res) => { //获取code
					const info = uni.getAccountInfoSync();
					const appId = info.miniProgram.appId;
					uni.login({
						provider: res.provider[0],
						success: (req) => {
							request.get({
								url: '/zzj_singleSaleApi/getOpenId',
								data: {
									js_code: req.code,
									appid: appId,
									bid: store.state.bid, 
									request:JSON.stringify(store.state.request),
									share_v_id: store.state.share_v_id
								}
							}).then(res => {
								if (res.code != 100) return pop.Toast(res.msg);
								const result = res.data;
								const obj = {
									openid: result.openid,
									session_key: result.session_key,
									userInfo: result.userInfo,
								    vid: result.vid
								}
								 uni.setStorageSync('vid', result.vid);
								uni.setStorageSync('openid', result.openid);
								uni.setStorageSync('buyState', result.buyState);
								store.commit('getUser', obj)
								uni.setStorage({
									key: "userinfo",
									data: result.userInfo
								})
								resolve(result.openid)
							})
						}
					})
				}
			})

		})
	},

}