import config from '../common/config.js';
import store from '@/store/index.js'
let systemData = {
	system: '', //操作系统版本
	platform: '', //操作系统类型  ios  android
	brand: '', //手机品牌
	model: '' //手机型号
}


let source = '' //访问端/来源 1微信公众号 2微信小程序 3支付宝小程序 4百度小程序 5头条小程序 6抖音小程序 7快应用
let regId = ''
uni.getSystemInfo({
	success: (res) => {
		uni.setStorageSync('systemData', res)
		systemData = res
	}
})




// #ifdef H5
let ua = window.navigator.userAgent.toLowerCase()
if (ua.match(/MicroMessenger/i) == 'micromessenger') {
	source = 1
} else {
	source = 8
}
// #endif
// #ifdef MP-WEIXIN
source = 2
// #endif
// #ifdef MP-ALIPAY
source = 3
// #endif
// #ifdef MP-BAIDU
source = 4
// #endif
// #ifdef MP-TOUTIAO
source = 6
// #endif
// #ifdef QUICKAPP-WEBVIEW-UNION || QUICKAPP-WEBVIEW-HUAWEI || QUICKAPP-WEBVIEW
source = 7
// #endif
// #ifdef APP-PLUS
source = 9
// #endif

function request (url, data, method)  {
    data = Object.assign({}, {
       source: 2,
	   system: systemData.system,
	   platform: systemData.platform,
	   brand: systemData.brand,
	   longitude: store.state.position.longitude,
	   latitude: store.state.position.latitude,
	   regId: regId,
	   v_id: data && data.v_id?data.v_id:store.state.vid,
	   vid: data && data.v_id?data.v_id:store.state.vid,
	   bid: data && data.bid?data.bid:store.state.bid,
    }, data)
	return new Promise((resolve, reject) => {
		uni.request({
			url: config.baseUrl + url,
			method: method,
			data,
			dataType: "json",
			header: {
				'content-type': 'application/x-www-form-urlencoded',
			},
			success: function(resp) {
				resolve(resp.data);
			},
			fail: function(resp) {
				reject(resp);
			}
		})
	})
}


function request_ds (url, data, method)  {
    data = Object.assign({}, {
       source: 2,
	   system: systemData.system,
	   platform: systemData.platform,
	   brand: systemData.brand,
	   longitude: store.state.position.longitude,
	   latitude: store.state.position.latitude,
	   regId: regId,
	   v_id:store.state.vid,
	   bid:store.state.bid,
    }, data)
	return new Promise((resolve, reject) => {
		uni.request({
			url: 'https://api.tianditu.gov.cn' + url,
			method: method,
			data,
			dataType: "json",
			header: {
				'content-type': 'application/x-www-form-urlencoded',
			},
			success: function(resp) {
				resolve(resp.data);
			},
			fail: function(resp) {
				reject(resp);
			}
		})
	})
}


function post(obj) {
    return request(obj.url, obj.data, 'POST');
}

function get(obj) {
    return request(obj.url, obj.data, 'GET');
}
function get_ds(obj) {
    return request_ds(obj.url, obj.data, 'GET');
}

export default {
    post,
    get,
	get_ds,
	source,
}