import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

const store = new Vuex.Store({
	state: {
		System: '',
		navWidth: '',
		navHeight: 50,
		totalHeight: '',
		capsuleWidth: '',
		statusBarHeight: '',
		title: '',
		service_tel: '',
		bid: 'YgfUPWCFjzOb9XxCDCOkeA==', // YgfUPWCFjzOb9XxCDCOkeA==  KiLtfQodXzQ1VJ98d0uy0g==
		color: '#287AF5',
		vid: '', // 100725675 100722547 100720530    100725612
		share_v_id: '',
		userInfo: '',

		logo: '',
		nameDefinition: '',
		withdrawal_rules: '', // 提现说明
		mode: 0, // 是否开起了分销
		share: '',
		App: '', // 小程序信息
		openid: '',
		session_key: '',
		position: { // 经纬度
			longitude: '',
			latitude: '',
		},
		cashPay: 0, // 是否开起线下付款 1开起 0不开启
		payState: 0, // 是否开起支付功能 1开起 0不开启
		resultAdd: '',
		articleInfo: '',
		cate_id: '',
		agreement: false,
		is_hidden: 0,
		regInfo: '',
		privacy: true, // 隐私提示
		scanCode: true, // 扫码
		Camera: true, // 相机权限
		callPhone: true, // 电话权限
		city: '重庆',
		StoreBind: true, // 是否绑定了管家
		cartName: '购物车'
	},
	mutations: {
		setCity(state, payload) {
			state.city = payload
		},
		getAddress(state, payload) { // 收货地址
			state.resultAdd = payload
		},
		getCashPay(state, payload) {
			state.cashPay = payload
		},
		getpayState(state, payload) {
			state.payState = payload
		},

		getNav(state, payload) {
			state.navWidth = payload.navWidth;
			state.navHeight = payload.navHeight;
			state.totalHeight = payload.totalHeight;
			state.capsuleWidth = payload.capsuleWidth;
			state.statusBarHeight = payload.statusBarHeight;
		},
		setSystem(state, payload) {
			state.System = payload;
		},
		setApplet(state, payload) {
			state.App = payload;
		},

		getUser(state, payload) {
			if (payload.vid || payload.vid == '') state.vid = payload.vid;
			if (payload.openid) state.openid = payload.openid;
			if (payload.session_key) state.session_key = payload.session_key;
			if (payload.share_v_id) state.share_v_id = payload.share_v_id;
			if (payload.share) state.share = payload.share;
			if (payload.logo) state.logo = payload.logo;
			if (payload.nameDefinition) state.nameDefinition = payload.nameDefinition;
			if (payload.withdrawal_rules) state.withdrawal_rules = payload.withdrawal_rules;
			if (payload.mode) state.mode = payload.mode;
			if (payload.userInfo) state.userInfo = payload.userInfo;
			if (payload.title) state.title = payload.title;
			if (payload.articleInfo) state.articleInfo = payload.articleInfo;
			if (payload.cate_id) state.cate_id = payload.cate_id;
			if (payload.is_hidden) state.is_hidden = payload.is_hidden;
			if (payload.regInfo) state.regInfo = payload.regInfo;
			if (payload.service_tel) state.service_tel = payload.service_tel;
		},
		setAgree(state, payload) {
			state.agreement = payload;
		},
		//  获取经纬度
		Position(state, payload) {
			state.position = payload;
		},
		changePrivacy(state, payload) {
			state.privacy = payload;
		},
		changeScanCode(state, payload) {
			state.scanCode = payload;
		},
		changeCamera(state, payload) {
			state.Camera = payload;
		},
		changeCallphone(state, payload) {
			state.callPhone = payload;
		},
		SetBindState(state, payload) {
			state.StoreBind = payload;
		}
	},

	plugins: [
		createPersistedState({
			key: 'YataiApp',
			paths: [
				'System',
				'navWidth',
				'navHeight',
				'totalHeight',
				'statusBarHeight',
				'capsuleWidth',
				'service_tel',
				'title',
				'bid',
				'vid',
				'share_v_id',
				'logo',
				'nameDefinition',
				'withdrawal_rules',
				'mode',
				'share',
				'App',
				'openid',
				'session_key',
				'position',
				'cashPay',
				'payState',
				'resultAdd',
				'articleInfo',
				'cate_id',
				'agreement',
				'is_hidden',
				'regInfo',
				'privacy',
				'scanCode',
				'Camera',
				'callPhone',
				'city',
				'StoreBind'
			],
			storage: {
				getItem: (key) => uni.getStorageSync(key), // 获取
				setItem: (key, value) => uni.setStorageSync(key, value), // 存储
				removeItem: (key) => uni.removeStorageSync(key) // 删除
			}
		})

	]
})

export default store