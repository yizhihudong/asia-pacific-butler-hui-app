import App from './App'

import http from '@/common/request.js'
Vue.prototype.$http = http;

import config from '@/common/config.js'
Vue.prototype.imgUrl = config.imgUrl

Vue.prototype.staticUrl = config.staticUrl


import common from '@/common/common.js'
Vue.prototype.$common = common
Vue.prototype.$back = common.back
Vue.prototype.back = common.back
Vue.prototype.call = common.call
Vue.prototype.tel = common.tel
Vue.prototype.toUrl = common.toUrl
Vue.prototype.rectTo = common.rectTo
Vue.prototype.toGoods = common.toGoods
Vue.prototype.$pageHome = common.pageHome
Vue.prototype.backHome = common.backHome
Vue.prototype.$pageTab = common.pageTab
Vue.prototype.$BarTitle = common.BarTitle
Vue.prototype.$previewImage_num = common.previewImage_num
Vue.prototype.$ImageNum = common.ImageNum


import pop from '@/common/pop.js'
Vue.prototype.$pop = pop;
Vue.prototype.$showToastTxt = pop.showToastTxt;
Vue.prototype.$Toast = pop.showToastTxt;
Vue.prototype.$showLoading = pop.showLoading;
Vue.prototype.$hideLoading = pop.hideLoading;
Vue.prototype.$Modal = pop.model;
Vue.prototype.$picture = pop.picture;
Vue.prototype.$imgUrls = pop.imgUrls;
Vue.prototype.$staticTab = pop.staticTab
Vue.prototype.$staticImg = pop.staticImg;
Vue.prototype.$tabbar = pop.tabbar;
Vue.prototype.$pageRela = pop.pageRela;
Vue.prototype.$getViewInfo = pop.getViewInfo;
Vue.prototype.$pageScrollTo = pop.pageScrollTo;
Vue.prototype.$openLocation = pop.openLocation;
Vue.prototype.$bannelPage = pop.bannelPage;
Vue.prototype.$PageUrl = pop.PageUrl;
Vue.prototype.$Details = pop.Details;
Vue.prototype.$toStore = pop.toStore;
Vue.prototype.$getLocation = pop.getLocation;
import reg from './common/reg.js'
Vue.prototype.$reg = reg
import uView from "uview-ui";
Vue.use(uView);

import store from '@/store/index.js'
Vue.prototype.$store = store




// #ifndef VUE3
import Vue from 'vue'
import './uni.promisify.adaptor'
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
	store,
  ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif